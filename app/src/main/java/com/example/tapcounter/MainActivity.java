/* William Moser
   COSC 3352.001
   Feb. 14, 2019
   Assn 2. This application increases and resets a displayed
   counter.
 */
package com.example.tapcounter;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;
import android.view.View;
public class MainActivity extends AppCompatActivity {

   TextView showValue;
   int counter = 0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        showValue = (TextView) findViewById(R.id.counterValue);
    }

    //function increases counter Value
    public void countIn(View view)
    {
        counter++;
        showValue.setText(Integer.toString(counter));
    }

    //function resets counter Value
    public void resetCount (View view)
    {
        counter = 0;
        showValue.setText(String.valueOf(counter));
    }
}
